ibevar_reformation = {
	potential = {
		tag = A32
	}

	can_start = {
		always = yes
	}
	
	
	can_stop = {
        always = no
	}
	
	progress = {
	}
	
	can_end = {
		all_owned_province = {
			religion = regent_court
		}
	}
	
	modifier = {
		land_morale = -0.1
		stability_cost_modifier = 0.5
	}
	
	on_start = ibevar.2
	on_end = ibevar.7
	
	on_monthly = {
		events = {
			ibevar.102
		}
		random_events = { 
		}
	}
}

