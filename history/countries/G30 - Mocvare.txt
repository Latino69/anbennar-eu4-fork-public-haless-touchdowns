government = monarchy
add_government_reform = feudalism_reform
government_rank = 1
primary_culture = dolindhan
religion = ynn_river_worship
technology_group = tech_ynnic
capital = 1188
historical_rival = G23 #Juzondezan
historical_rival = G32 #Malacnar

1000.1.1 = { set_estate_privilege = estate_mages_organization_guilds }

1423.1.1 = {
	monarch = {
		name = "Pjodorn II 'the Good'"
		dynasty = "yen Ceped"
		birth_date = 1403.1.1
		death_date = 1450.1.1
		adm = 4
		dip = 2
		mil = 5
	}
	add_ruler_personality = secretive_personality
	add_ruler_personality = cruel_personality
	add_ruler_personality = strict_personality
}